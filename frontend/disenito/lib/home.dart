import 'package:disenito/views/editReg.dart';
import 'package:disenito/widgets/customText.dart';
import 'package:flutter/material.dart';
import 'package:responsive_grid_list/responsive_grid_list.dart';

class Home extends StatefulWidget {
  const Home({super.key, required this.title});
  final String title;

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: ResponsiveGridList(
            verticalGridMargin: 30,
            horizontalGridMargin: 30,
            minItemWidth: 500,
            children: List.generate(5, (index) {
              return Card(
                  child: Container(
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: color_(index),
                          borderRadius: BorderRadius.circular(10.0)),
                      height: 500,
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(text: 'Tipo de evento:'),
                          CustomText(text: 'Lugar:'),
                          CustomText(text: 'Nombre del evento:'),
                          CustomText(text: 'Descripcion:'),
                          CustomText(text: 'Fecha y hora:'),
                          CustomText(text: 'Duración:'),
                          CustomText(text: 'Edificio:'),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              IconButton(
                                  onPressed: () {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content:
                                            Text('El evento se esta publicando...'),
                                      ),
                                    );
                                  },
                                  color: Colors.white,
                                  icon: Icon(Icons.check)),
                              IconButton(
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => EditReg(),
                                      ),
                                    );
                                  },
                                  color: Colors.white,
                                  icon: Icon(Icons.edit)),
                              IconButton(
                                  onPressed: () {
                                    ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content:
                                            Text('El evento se esta eliminando...'),
                                      ),
                                    );
                                  },
                                  color: Colors.white,
                                  icon: Icon(Icons.delete)),
                            ],
                          )
                        ],
                      )));
            })));
  }
}

Color color_(num) {
  return num >= 3 ? Colors.grey : Colors.purple;
}
