import 'package:disenito/home.dart';
import 'package:disenito/widgets/customText.dart';
import 'package:flutter/material.dart';

class EditReg extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Editar registro'),
      ),
      body: Container(
        padding: EdgeInsets.all(30.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text('Tipo de evento:'),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                hintText: 'Introduce el tipo de evento',
              ),
            ),
            SizedBox(height: 16.0),
            Text('Lugar:'),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                hintText: 'Introduce el lugar',
              ),
            ),
            SizedBox(height: 16.0),
            Text('Nombre del evento:'),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                hintText: 'Introduce el nombre del evento',
              ),
            ),
            SizedBox(height: 16.0),
            Text('Descripción:'),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                hintText: 'Introduce la descripción',
              ),
            ),
            SizedBox(height: 16.0),
            Text('Fecha y hora:'),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                hintText: 'Introduce la fecha y hora',
              ),
            ),
            SizedBox(height: 16.0),
            Text('Duración:'),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                hintText: 'Introduce la duración del evento',
              ),
            ),
            SizedBox(height: 16.0),
            Text('Edificio:'),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                hintText: 'Introduce el edificio',
              ),
            ),
            SizedBox(height: 16.0),
            Container(
              margin: EdgeInsets.all(5),
              padding: EdgeInsets.all(5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => Home(
                                title: '',
                              ),
                            ),
                          );
                        },
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.purple)),
                        child: Text('Guardar', style: TextStyle(color: Colors.white, fontSize: 15),)),
                    TextButton(
                        onPressed: () {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              content: Text('El evento se esta actualizando...'),
                            ),
                          );
                        },
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.purple)),
                        child: Text('Guardar', style: TextStyle(color: Colors.white, fontSize: 15),)),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
